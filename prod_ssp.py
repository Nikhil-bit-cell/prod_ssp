import os

import psutil
import requests
import pandas as pd
from datetime import datetime, timedelta
from time import localtime, strftime
import time
#import SNOWFLAKE_HELPER as snowflakeconnection

import pandas as pd

from setting import CONFIG
import findspark
findspark.init()
import pyspark.pandas as ps
process = psutil.Process(os.getpid())




class Ssp():

    def __init__(self):
        self.current_path = os.getcwd()
        # chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_experimental_option("prefs",{"download.default_directory":"C:\\Users\\nikhil.chouhan\\Documents\\project\\prod_ssp\\downloads"})
        # self.driver_path = os.path.join(os.getcwd(), 'chromedriver.exe')
        

    def _get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        if resp.status_code != 200:
            pass
            
        resp.raise_for_status()
        csv= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\cases.csv")
        case = 'stateestimator_202211151700'
        return case
        
    def miso_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        csv= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\branch-mappings.csv")
        return csv

    def contingencies_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        df_ctgs= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\contingencies.csv")
        return df_ctgs


    def bus_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        all_bus= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\buses.csv")
        return all_bus
    
    def spp_dam_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        const_list_1a= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\binding_constraints.csv")
        return const_list_1a
    
    def transient_branches_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        transient_branches_raw= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\branches.csv")
        return transient_branches_raw
    
    def case_branches_get_dataframe(self,url, auth):
        resp = requests.get(url, auth=auth)
        # if resp.status_code != 200:
        #     pass
            
        transient_branches_raw= ps.read_csv(r"C:\\Users\\nikhil.chouhan\\Downloads\\branches (1).csv")
        return transient_branches_raw

    def cases(self):
        d = {'RPTG_ISO': ['SPPISO'],'MARKET_TYPE':['DA'], 'RUN_DATE':[strftime("%Y-%m-%d %H:%M:%S", localtime())]}
        df = pd.DataFrame(data=d)
        j1=df.to_json(orient='records')
        try:
            case = self._get_dataframe(CONFIG['url'], CONFIG["RMS_AUTH"])
            filelist = [ f for f in os.listdir(r"C:\\Users\\nikhil.chouhan\\Projects\\spp") if f.endswith(".parquet") ]
            for f in filelist:
                os.remove(os.path.join(r"C:\\Users\\nikhil.chouhan\\Projects\\spp", f))

            # srikant 06102020 placed below code before filelist code to avoid deletion of temp folder
            dir2 = 'C:\\temp\\DA'
            if not os.path.exists(dir2):
                os.makedirs(dir2)
                
            filelist = [ f for f in os.listdir(r"C:\\temp\\DA") if f.endswith(".csv") ]
            for f in filelist:
                os.remove(os.path.join(r"C:\\temp\\DA", f)) 
            return case
        except:
            pass
    
    #Get Branch_Mapping that maps the latest MISO Constraint ID to the branches
    def MISO_Constraint(self,case):
        url = f'https://api1.marginalunit.com/rms/spp/binding-constraints/spp-se/{case}/branch-mappings'
        df_rms_mapping = self.miso_get_dataframe(url, CONFIG['RMS_AUTH'])
        df_rms_mapping=df_rms_mapping.rename(columns = {'branch_name':'branch_uid'})

        return df_rms_mapping

    #This pulls all the contingencies
    def pulls_all_the_contingencies(self,case):
        url = f'https://api1.marginalunit.com/reflow/spp-se/{case}/contingencies'
        df_ctgs = self.contingencies_get_dataframe(url, CONFIG['RMS_AUTH'])
        return df_ctgs

    #Get the Bus data to identify the From/To Area
    def bus_data(self,case):
        url_998=f'https://api1.marginalunit.com/reflow/spp-se/{case}/buses'
        all_bus = self.bus_get_dataframe(url_998, CONFIG['RMS_AUTH'])
        all_bus=all_bus.groupby(['name','base_kv']).head(1)
        return all_bus

    #fromcase = 'SPP_DAm_se_20200412_H01'
    def spp_dam(self,date_begin,df_rms_mapping):
        url='https://api1.marginalunit.com/misc-data/spp/binding_constraints?market=da&start=' + '2018-01-01 00:00:00' + '&end=' + str(date_begin.year) + '-' + str(date_begin.month).rjust(2,'0') + '-' + str(date_begin.day).rjust(2,'0') + ' 00:00:00'
        const_list_1a = self.spp_dam_get_dataframe(url, CONFIG['RMS_AUTH'])
        
        const_list_1a=const_list_1a.rename(columns = {'contingency_name':'contingency'})
        const_list_b = ps.merge(const_list_1a,df_rms_mapping,how = 'left', left_on=['constraint_name', 'contingency','monitored_facility'],right_on=['constraint_name', 'constraint_contingent_facility','constraint_monitored_facility'])
        const_list_b = const_list_b[const_list_b['convention'].notnull()]
        const_list_b = const_list_b[const_list_b['branch_uid'].notnull()]
        const_list_b = const_list_b.reset_index(drop=True)
        basecase_cnx = const_list_b[const_list_b['contingency'] == 'BASE CASE']
        other_cnx = const_list_b[const_list_b['contingency'] != 'BASE CASE']
        other_cnx_with_ctg_1 = ps.merge(other_cnx, df_ctgs.drop_duplicates(subset=['contingency_name']), how='left',left_on=['contingency'],right_on=['contingency_name'])
        other_cnx_with_ctg = other_cnx_with_ctg_1.dropna(subset=['contingency_name'])
        other_cnx_with_ctg = other_cnx_with_ctg[['monitored_facility','contingency', 'convention', 'branch_uid']]
        const_list = basecase_cnx.append(other_cnx_with_ctg)
        const_list.loc[const_list.convention == 0,'convention'] = 1
        const_list = const_list.reset_index(drop=True)
        return const_list_1a,const_list

    #Find Transient Outages in Latest Case and create a single string variable with comma delimited
    #If last_closed populated, those are TRANSIENT outages and need to be energized 
    def Transient_Outages(self,case):
        url=f'https://api1.marginalunit.com/reflow/spp-se/{case}/branches?columns=name,last_closed'
        transient_branches_raw = self.transient_branches_get_dataframe(url, CONFIG['RMS_AUTH'])
        transient_branches = transient_branches_raw.dropna()
        return transient_branches

    #Out of these branches, which ones are out in the REFERENCE case and only energize those
    #Get Branches From Latest Case
    def Branches_From_Latest_Case(self,case,transient_branches,const_list):
        url=f'https://api1.marginalunit.com/reflow/spp-se/{case}/branches'
        case_branches = self.case_branches_get_dataframe(url, CONFIG['RMS_AUTH'])
        case_branches = case_branches.drop_duplicates(subset = 'name',keep=False)
        ref_case_OOS = case_branches[case_branches['status']==False].name
        transient_branches = transient_branches[transient_branches.name.isin(list((ref_case_OOS.to_numpy())))]
        transient_branches=','.join(list(transient_branches['name'].unique().to_numpy()))
        const_list = ps.merge(const_list,case_branches,how = 'inner', left_on=['branch_uid'], right_on=['name'])
        const_list = const_list.rename(columns = {'from_bus_name':'from_station_name','from_bus_kv':'from_station_voltage','to_bus_name':'to_station_name','to_bus_kv':'to_station_voltage'})
        





    






    
if __name__=='__main__':
    start_time = time.time()
    self=Ssp()
    case=self.cases()
    df_rms_mapping=self.MISO_Constraint(case)
    df_ctgs=self.pulls_all_the_contingencies(case)
    all_bus=self.bus_data(case)
    date_begin= datetime.today() + timedelta(1) 
    fromcase = (datetime.strptime((case.split('_',4)[1][0:8]),"%Y%m%d") - timedelta(7)).strftime("%Y%m%d")
    const_list_1a,const_list=self.spp_dam(date_begin,df_rms_mapping)
    transient_branches=self.Transient_Outages(case)
    self.Branches_From_Latest_Case(case,transient_branches,const_list)
    seconds = time.time() - start_time
    print("________________________________________________________________________________")
    print('Time Taken:', time.strftime("%H:%M:%S",time.gmtime(seconds)))
    
